#!/usr/bin/env python
# Description:
# This rosnode is to execute general formation of multi-agent systems.
# Created by:
# 1. Hilton Tnunay (htnunay@gmail.com)
# 2. Zhenhong Li (lizhenhong110@gmail.com)
#
# Subscribed topics:
# 1. Position of all robots: /vicon/Mona%d/Mona%d
#    Message: geometry_msgs/TransformStamped
#
# Published topics:
# 1. Velocity of <i>-th robot: /epuck_robot_<i>/mobile_base/cmd_vel, for <i>=1,...,N
#	 Message: geometry_msgs/Twist

from __future__ import division
from __future__ import print_function
import rospy
import math
import numpy as np
import scipy as sp
import sys
from geometry_msgs.msg import TransformStamped as geoTrStamp
from geometry_msgs.msg import Pose2D as geoPose2D
from geometry_msgs.msg import Twist as geoTwist
from tf.transformations import euler_from_quaternion
from std_msgs.msg import String as stdString
from std_msgs.msg import Float32MultiArray as stdFloat32
import connectivity as net
import Save_Read_Files as SRF


# Some constants defined here
factor = 0.1
NRobot = 7
NRobot_leader = 3
NRobot_follower = 4
RRobot = 0.1 * factor
RR = 0
dim = 2
kp = 6.5
ki = 6
alpha = 14005.6 #0.5 * 100 / factor   #alpha is used to rescale the difference between velocity and encoder ticks
beta = 0.8*66.4187
t_wait = 2 # the system will wait t_wait sec before start to behavor.
           # This period gives enough time to measure the inital position of the robot
run_times = 0  # count- the while loop excute


# parameter for pre-specified formation control
gain_a = 0.12*0.9
gain_b = 0.4*0.8
T_settle = 100 #prescribed converge time /sec
# velocity offset of two leaders
leader_index = np.array([4,5,6])

# velOffset=np.array([[0.005],
#                   [0.00]]) #m/s

velOffset = np.array([[0.0],
                      [0.0]])

# Kinematics constrains
limitVel = 0.04 * 1000000  # cm/s
limitOmega = np.pi / 4 * 0.8*10000000

# designed position
position_d = np.zeros([NRobot*dim,1])

# position_d = np.array([[0.20],  # follower 1 x
#                        [0.20],  # follower 1 y
#                        [0.20],  # follower 2 x
#                        [-0.30],  # follower 2 y
#                        [-0.30],  # follower 3 x
#                        [-0.30],  # follower 3 y
#                        [-0.30],  # follower 4 x
#                        [0.20],  # follower 4 y
#                        [0.70],  # leader 1 x
#                        [0.20],  # leader 1 y
#                        [0.70],  # leader 2 x
#                        [-0.30]])  # leader 2 y

# obstacles
# ---------------------------
# circle
circle_c = np.array([[0.40],
                     [0.70]])  # center point of circle
radius = 0.15  # radius of circle
activate_range = 0.08
# -------------------------
# trajectory obstacle
line_end = 0.4  # end point of obstacle area

# Robot's info
IDRobot = np.arange(NRobot, dtype=np.uint8)
robotPose = [geoPose2D() for i in range(NRobot)]
robotInput = [geoTwist() for i in range(NRobot)]

update_rate = 60 # 1-80 hz according to the PID level of the mona
# Global variables
u = np.zeros([dim, 1])

# Function: transforming quaternion to euler angle
def geoTrStamp_to_Pose2D(poseData):
    robotPose2D = geoPose2D()
    orientation_list = [poseData.transform.rotation.x, poseData.transform.rotation.y, poseData.transform.rotation.z,
                        poseData.transform.rotation.w]
    (roll, pitch, yaw) = euler_from_quaternion(orientation_list)
    robotPose2D.theta = yaw
    robotPose2D.x = poseData.transform.translation.x
    robotPose2D.y = poseData.transform.translation.y

    return robotPose2D


# Function: pose callback from camera
def cameraPoseCallback(poseData, index):
    robotPose[index] = geoTrStamp_to_Pose2D(poseData)


# Function: main operation
def main(args):
    global run_times
    rospy.init_node('monacom_formation', anonymous=True)

    # declaring ros subscriber and publisher
    subCameraPose = [rospy.Subscriber("vicon/Mona%d/Mona%d" % (i,i), geoTrStamp, cameraPoseCallback, i) for i in range(NRobot)]
    pubCommand = [rospy.Publisher("mona%d/cmd_vel" % i, geoTwist, queue_size=1) for i in range(NRobot_follower)]

    # setting ros update rate
    rate = rospy.Rate(update_rate)  

    net.updateTopology(NRobot, dim)
    # Adj = np.array([[0, 1, 1, 1, 1, 1, 1],
    # 				[1, 0, 1, 1, 1, 1, 1],
    # 				[1, 1, 0, 1, 1, 1, 1],
    # 				[1, 1, 1, 0, 1, 1, 1],
    #                 [1, 1, 1, 1, 0, 1, 1],
    #                 [1, 1, 1, 1, 1, 0, 1],
    #                 [1, 1, 1, 1, 1, 1, 0]])
    # Adj = np.array([[0, 1, 1, 1, 1, 1, 1],
    # 				[1, 0, 1, 1, 1, 1, 1],
    # 				[1, 1, 0, 1, 1, 1, 1],
    # 				[1, 1, 1, 0, 1, 1, 1],
    #                 [1, 1, 1, 1, 0, 1, 1],
    #                 [1, 1, 1, 1, 1, 0, 1],
    #                 [1, 1, 1, 1, 1, 1, 0]])
    Stress = np.array([[-0.7524, 0.0684, 0.2052, 0, -0.1368, 0.6156, 0],
                       [0.0684, -0.7524, 0, 0.2052, -0.1368, 0, 0.6156],
                       [0.2052, 0, -0.2052, 0.1026, 0, 0, -0.1026],
                       [0, 0.2052, 0.1026, -0.2052, 0, -0.1026, 0],
                       [-0.1368, -0.1368, 0, 0, -0.1368, 0.2052, 0.2052],
                       [0.6156, 0, 0, -0.1026, 0.2052, -0.7182, 0],
                       [0, 0.6156, -0.1026, 0, 0.2052, 0, -0.7182]])
    # Adj = np.array([[0, 1, 1, 1, 1, 1],
    #                 [1, 0, 1, 0, 1, 0],
    #                 [1, 1, 0, 1, 0, 0],
    #                 [1, 0, 1, 0, 0, 0],
    #                 [1, 0, 0, 0, 0, 1],
    #                 [1, 1, 0, 0, 1, 0]])
    # Adj = np.array([[0, 1, 1, 1, 1, 1],
    # 				[1, 0, 1, 1, 1, 1],
    # 				[1, 1, 0, 1, 1, 1],
    # 				[1, 1, 1, 0, 1, 1],
    #                 [1, 1, 1, 1, 0, 1],
    #                 [1, 1, 1, 1, 1, 0]])

    # net.setAdjacency(Adj)
    # net.setLaplacian(Stress)
    net.setStress(Stress)
    net.setOffset(position_d)
    # net.setEandG_star(position_d)
    # net.setG_star_sum()

    tStart = rospy.get_time()
    mode = 0
    while not rospy.is_shutdown():
        t = (rospy.get_time() - tStart)    # time from the program start
        if (rospy.get_time() - tStart) > t_wait:
            running_time = t - t_wait   # time after the intialization
            SRF.SaveFile.save_file(float(running_time), 'time', './data/form', run_times)


            for i in range(NRobot):
                net.setState_i(i, np.array([[robotPose[i].x], [robotPose[i].y]]))

                SRF.SaveFile.save_file(float(robotPose[i].x), 'X_robot' + str(i), './data/form', run_times)  # Save the x y and theta data
                SRF.SaveFile.save_file(float(robotPose[i].y), 'Y_robot' + str(i), './data/form', run_times)
                SRF.SaveFile.save_file(float(robotPose[i].theta), 'Theta_robot' + str(i), './data/form', run_times)

            # net.setEandG_matrix()
            # net.setG_sum()
            # net.SetG_proj_matrix()
            # net.SetG_proj_sum()

            # position_e = net.calPosition_e(position_d)
            net.calculateAffineController(NRobot_follower)
            position_e = net.calculateStateError()
            SRF.SaveFile.save_file(float(position_e), 'Position_error', './data/form', run_times)

            gain_t_v = net.time_varying_gain(gain_a, gain_b, T_settle, running_time)

            for i in range(NRobot_follower):
                # u = net.getBearingformation_proj(kp, i)
                # u = net.getBearingformation_proj(gain_t_v, i)
                # u = net.getConsensus(kp, i, velOffset)
                u = net.getAffineController(kp, i, velOffset)
                ru = net.getControllerKinematics(alpha, beta, u, robotPose[i].theta, RRobot, limitVel, limitOmega)
                robotInput[i].linear.x = ru[0]
                robotInput[i].angular.z = ru[1]

                pubCommand[i].publish(robotInput[i])

                SRF.SaveFile.save_file(float(u[0]), 'X_velocity' + str(i), './data/form', run_times)
                SRF.SaveFile.save_file(float(u[1]), 'Y_velocity' + str(i), './data/form', run_times)
                SRF.SaveFile.save_file(float(ru[0]), 'Linear_v_robot' + str(i), './data/form', run_times)
                SRF.SaveFile.save_file(float(ru[1]), 'Angular' + str(i), './data/form', run_times)

            word = "T: " + str(t) + ", Position Error: " + str(position_e) + \
                   ", Time-varying gain: " + str(gain_t_v)
            rospy.loginfo(word)
            run_times = run_times + 1
        rate.sleep()


if __name__ == '__main__':
    main(sys.argv)
