#!/usr/bin/env python
# Description:
# This rosnode is to execute general formation of multi-agent systems.
# Created by:
# 1. Hilton Tnunay (htnunay@gmail.com)
# 2. Zhenhong Li (lizhenhong110@gmail.com)
#
# Subscribed topics:
# 1. Position of all robots: /vicon/Mona%d/Mona%d
#    Message: geometry_msgs/TransformStamped
#
# Published topics:
# 1. Velocity of <i>-th robot: /epuck_robot_<i>/mobile_base/cmd_vel, for <i>=1,...,N
#	 Message: geometry_msgs/Twist

from __future__ import division
from __future__ import print_function
import rospy
import sys
import math
import numpy as np
from geometry_msgs.msg import TransformStamped as geo_tr_stamp
from geometry_msgs.msg import Pose2D as geo_pose_2D
from geometry_msgs.msg import Twist as geo_twist
from tf.transformations import euler_from_quaternion as quat_to_euler

import network as netw
import controller as contr

# Network's agents and state dimension
n_robot = 7
n_leader = 3
n_follower = 4
dim = 2 # Dimension of position
dim_ext_state = 1 # Dimension of the heading angle

kp = 6.5

vel_offset = np.array([[0.0],
                       [0.0]])

cmd = np.zeros([dim, 1])

des_pos = np.zeros([n_robot*dim, 1])

stress = np.array([[-0.7524, 0.0684, 0.2052, 0, -0.1368, 0.6156, 0],
                   [0.0684, -0.7524, 0, 0.2052, -0.1368, 0, 0.6156],
                   [0.2052, 0, -0.2052, 0.1026, 0, 0, -0.1026],
                   [0, 0.2052, 0.1026, -0.2052, 0, -0.1026, 0],
                   [-0.1368, -0.1368, 0, 0, -0.1368, 0.2052, 0.2052],
                   [0.6156, 0, 0, -0.1026, 0.2052, -0.7182, 0],
                   [0, 0.6156, -0.1026, 0, 0.2052, 0, -0.7182]])

# Robot's info and ros declaration
id_robot = np.arange(n_robot, dtype=np.uint8)
robot_pose = [geo_pose_2D() for i in range(n_robot)]
robot_cmd = [geo_twist() for i in range(n_robot)]

# Time variables
update_rate = 60
t_wait = 2.0
t_run = 0

# Function: transforming quaternion to euler angle
def geo_tr_stamp_to_pose_2D(_pose):
    robot_pose_2D = geo_pose_2D()
    ori_list = [_pose.transform.rotation.x, _pose.transform.rotation.y, _pose.transform.rotation.z,
                        _pose.transform.rotation.w]
    (roll, pitch, yaw) = quat_to_euler(ori_list)
    robot_pose_2D.theta = yaw
    robot_pose_2D.x = _pose.transform.translation.x
    robot_pose_2D.y = _pose.transform.translation.y

    return robot_pose_2D

# Function: pose callback from camera
def cam_pose_callback(_pose, _index):
    robot_pose[_index] = geo_tr_stamp_to_pose_2D(_pose)

# Function: main operation
def main(args):
    rospy.init_node('run_formation', anonymous=True)

    # Declaring ros subscriber and publisher
    sub_cam_pose = [rospy.Subscriber("vicon/Mona%d/Mona%d" % (i,i), geo_tr_stamp, cam_pose_callback, i) for i in range(n_robot)]
    pub_cmd = [rospy.Publisher("mona%d/cmd_vel" % i, geo_twist, queue_size=1) for i in range(n_follower)]

    # Setting ros update rate
    rate = rospy.Rate(update_rate)

    net = netw.network(n_robot, n_leader, n_follower)
    net.stress = stress

    ctrl = contr.controller(net, contr.affine, dim, dim_ext_state)
    ctrl.offset = des_pos

    t_start = rospy.get_time()
    while not rospy.is_shutdown():
        t = rospy.get_time() - t_start
        if t > t_wait:
            run_time = t - t_wait

            for i in range(n_robot):                
                ctrl.update_state_i(i, np.array([[robot_pose[i].x], [robot_pose[i].y]]))

            ctrl.update_command()
            err_norm = ctrl.calculate_state_error()

            for i in range(n_follower):            
                cmd = ctrl.update_command_and_kinematics(i, kp, vel_offset, robot_pose[i].theta)
                robot_cmd[i].linear.x = cmd[0]
                robot_cmd[i].angular.z = cmd[1]

                pub_cmd[i].publish(robot_cmd[i])
            
            word = "T: " + str(t) + ", State Error: " + str(err_norm)
            rospy.loginfo(word)
            run_time = run_time + 1
        rate.sleep()


if __name__ == '__main__':
    main(sys.argv)