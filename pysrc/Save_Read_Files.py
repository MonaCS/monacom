"""
-------------------------------------------------------------------------------------
Save and read files with data in the types of list, dictionary and tuple.
-------------------------------------------------------------------------------------
"""
import os
import json
import matplotlib.pyplot as plt
from matplotlib.figure import Figure


class SaveFile(object):

    @staticmethod
    def save_file(var, var_name, fold_path, run_times):
        """
        ------------------------------------------------------------------------------
        var: the variable (format: dict, list, tuple), a user would like to store
            in the .txt format
        var_name: a new name of the .txt file
        fold_path: the new folder should be created under the path:
            fold_path='./data/S1'
        ------------------------------------------------------------------------------
        """
        # the path to store the variable will be:
        # s_path='./data/S1/dic.txt'

        # create the new folder in he specified path
        SaveFile.make_dir(fold_path)
        # establish the path of the variable file
        s_path = fold_path + '/' + var_name + '.txt'
        # create the text file with the name of var under the new folder
        #   and open it in the writing mode
        if not run_times == 0:
            data_file = open(s_path, 'a')
        else:
            data_file = open(s_path, 'w')
        # convert the variable var to string before store it in the
        x = json.dumps(var)
        # write the data
        data_file.write(x)
        data_file.write('\n')
        #print('Data: ' + s_path + ' is saved successfully.')
        # close the data file
        data_file.close()

    # @staticmethod
    # def figure_saving_and_tracing(path, format_: str='png', handle: Figure=None):
    #     if handle is None:
    #         plt.savefig(path, format=format_)
    #     else:
    #         handle.savefig(path, format_=format_)
    #     print('Figure: ' + path + ' is saved successfully.')

    @staticmethod
    def make_dir(path):
        
        path = path.strip()
        
        is_exist = os.path.exists(path)

        if not is_exist:

            #print(path + ' is established.')

            os.makedirs(path)
            return True
        else:

            #print(path + ' already exists.')
            return False


class ReadFile(object):

    @staticmethod
    def read_file(var_name, fold_path):
        # specify the path of the variable file
        s_path = fold_path + '/' + var_name + '.txt'
        # open the text file with the name of dic under the new folder
        #   and specify it with the reading mode
        data_file = open(s_path, 'r')
        return json.load(data_file)
