#!/usr/bin/env python
# Description:
# This library contains the cooperative control of multi-agent systems.
# Created by:
# 1. Hilton Tnunay (htnunay@gmail.com)
# 2. Zhenhong Li (lizhenhong110@gmail.com)

from __future__ import division
from __future__ import print_function
import math
import numpy as np
import scipy as sp
import sys
import Save_Read_Files as SRF

class controller:
    def __init__(self, _network, _controller_name, _num_state, _num_ext_state = 0):
        self.network = _network
        self.control = _controller_name(_network)
        self.numberOfState = _num_state
        self.numberOfExtState = _num_ext_state

        self.state = np.zeros([_network.numberOfNode*_num_state, 1])
        self.ext_state = np.zeros([_network.numberOfNode*_num_ext_state, 1])
        self.offset = np.zeros([_network.numberOfNode*_num_state, 1])
        self.command = np.zeros([_network.numberOfNode*_num_state, 1])
        self.dyn_command = np.zeros([_network.numberOfNode*_num_state, 1])
    
    def update_state_i(self, _index, _state, _ext_state=None):
        self.state[self.numberOfState*_index : self.numberOfState*_index+self.numberOfState] = _state
        self.ext_state[self.numberOfExtState*_index : self.numberOfExtState*_index+self.numberOfExtState] = _ext_state
    
    def update_command(self):
        (self.offset, self.command) = self.control.run(self)
    
    def get_command(self, _index, _kp, _cmd_offset):
        command = _kp*self.command[self.numberOfState*_index : self.numberOfState*_index+self.numberOfState] + _cmd_offset
        return command

    def update_kinematics(self, _command, _heading, _limit_lin_vel=(0.04*1000000), _limit_ang_vel=(np.pi/4*0.8*10000000), _radius_robot=0.01, _alpha=14005.6, _beta=(0.8*66.4187)):
        linear_vel = _alpha*(_command[0]*math.cos(_heading) + _command[1]*math.sin(_heading))
        angular_vel = (_beta/_radius_robot)*(-_command[0]*math.sin(_heading) + _command[1]*math.cos(_heading))
        
        linear_vel = min(max(linear_vel, -_limit_lin_vel), _limit_lin_vel)
        angular_vel = min(max(angular_vel, -_limit_ang_vel), _limit_ang_vel)
        
        return np.array([linear_vel, angular_vel])

    def update_command_and_kinematics(self, _index, _kp, _cmd_offset, _heading):
        command = self.get_command(_index, _kp, _cmd_offset)
        cmd_velocity = self.update_kinematics(command, _heading)

        self.dyn_command[self.numberOfState*_index : self.numberOfState*_index+self.numberOfState] = cmd_velocity        

        return cmd_velocity
        
    def calculate_state_error(self):
        error = self.state - self.offset
        error_norm = np.linalg.norm(error, 2)
        return error_norm

    def save_data_to_file(self, _counter, _run_time_value, _pose_flag=True, _command_flag=True, _error_flag=True):
        SRF.SaveFile.save_file(float(_run_time_value), 'time', './data/formation', _counter)

        if _pose_flag:
            for i in range(self.network.numberOfNode):
                state = self.state[self.numberOfState*_index : self.numberOfState*_index+self.numberOfState]
                ext_state = self.ext_state[self.numberOfExtState*_index : self.numberOfExtState*_index+self.numberOfExtState]
                command = self.command[self.numberOfState*_index : self.numberOfState*_index+self.numberOfState]

                # Storing position and heading data                
                SRF.SaveFile.save_file(float(state[0]), 'Pose_x' + str(i), './data/formation', _counter)
                SRF.SaveFile.save_file(float(state[1]), 'Pose_y' + str(i), './data/formation', _counter)
                SRF.SaveFile.save_file(float(ext_state), 'Pose_ori' + str(i), './data/formation', _counter)

                SRF.SaveFile.save_file(float(command[0]), 'Cmd_x' + str(i), './data/formation', _counter)
                SRF.SaveFile.save_file(float(command[1]), 'Cmd_y' + str(i), './data/formation', _counter)
                SRF.SaveFile.save_file(float(dyn_command[0]), 'Vel_lin' + str(i), './data/formation', _counter)
                SRF.SaveFile.save_file(float(dyn_command[1]), 'Vel_ang' + str(i), './data/formation', _counter)

class affine:
    def __init__(self, _network):
        self.net = _network

    def run(self, _controller):
        state = _controller.state
        offset = _controller.offset        

        stress_ff = self.net.stress[0:self.net.numberOfFollower, 0:self.net.numberOfFollower]
        stress_fl = self.net.stress[0:self.net.numberOfFollower, self.net.numberOfFollower:self.net.numberOfNode]        

        invStress_ff = np.linalg.inv(stress_ff)
        invStress_ff_fl = np.matmul(invStress_ff, stress_fl)
        aug_stress_ff_fl = np.kron(invStress_ff_fl, np.eye(_controller.numberOfState))

        des_state_l = state[self.net.numberOfFollower*_controller.numberOfState:self.net.numberOfNode*_controller.numberOfState]
        des_state_f = -np.matmul(aug_stress_ff_fl, des_state_l)

        offset[0 : self.net.numberOfFollower*_controller.numberOfState] = des_state_f
        offset[self.net.numberOfFollower*_controller.numberOfState : self.net.numberOfNode*_controller.numberOfState] = state[self.net.numberOfFollower*_controller.numberOfState:self.net.numberOfNode*_controller.numberOfState]

        aug_stress_ff = np.kron(stress_ff, np.eye(_controller.numberOfState))
        state_f = state[0 : self.net.numberOfFollower*_controller.numberOfState]
        delta_state_f = state_f - des_state_f
        command = np.matmul(aug_stress_ff, delta_state_f)

        return (offset, command)


class consensus:
    def __init__(self, _network):
        self.net = _network

    def run(self, _controller):
        state = _controller.state
        offset = _controller.offset

        laplacian = self.net.laplacian

        delta_state = state - offset
        command = -np.matmul(laplacian, delta_state)

        return offset, command