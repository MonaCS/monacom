from __future__ import division
from __future__ import print_function
import numpy as np
import scipy as sp
import sys as ss
import Network_ZH as net

global g_star

factor = 0.1
NRobot = 6
NRobot_follower = 4
RRobot = 0.1 * factor
RR = 0
dim = 2

position_d = np.array([[1.30],  # follower 1 x
                       [0.50],  # follower 1 y
                       [1.30],  # follower 2 x
                       [0.30],  # follower 2 y
                       [0.60],  # follower 3 x
                       [0.30],  # follower 3 y
                       [0.60],  # follower 4 x
                       [0.50],  # follower 4 y
                       [1.80],  # leader 1 x
                       [0.50],  # leader 1 y
                       [1.80],  # leader 2 x
                       [0.30]])  # leader 2 y

net.updateTopology(NRobot, dim)
Adj = np.array([[0, 1, 1, 1, 1, 1],
                [1, 0, 1, 0, 1, 0],
                [1, 1, 0, 1, 0, 0],
                [1, 0, 1, 0, 0, 0],
                [1, 0, 0, 0, 0, 1],
                [1, 1, 0, 0, 1, 0]])
# Adj = np.array([[0, 1, 1, 1, 1, 1],
# 				[1, 0, 1, 1, 1, 1],
# 				[1, 1, 0, 1, 1, 1],
# 				[1, 1, 1, 0, 1, 1],
#                    [1, 1, 1, 1, 0, 1],
#                    [1, 1, 1, 1, 1, 0]])

# Adj = np.array([[ 0, 1,0, 1],
# 				[ 1, 0, 1, 1],
# 				[0, 1, 0, 1],
# 				[ 1,1, 1, 0]])

net.setAdjacency(Adj)
net.setEandG_star(position_d)
net.setG_star_sum()
net.setEandG_matrix()
net.setG_sum()
net.SetG_proj_matrix()
net.SetG_proj_sum()

# print(net.g_matrix)
# print(net.g_star)
# print(net.g_sum_n)

a1 = net.g_matrix[2:4, 4]
a2 = net.g_star[2:4, 4]

a3 = net.bearing_projection(a1)

q = np.matmul(net.bearing_projection(a1), a2)

print(q)

print(net.g_projected)
#print(net.g_projected_sum_n)

# global x
#
# x = 0
#
# def sum():
#     x = 2
#     return x
#
# u = sum()
#
# print(a1)
# print(2*a1)


# a5 = net.calPosition_e(position_d)
# a6 = np.linalg.norm(net.State - position_d)
# print(a6)
# print(a5)
#
# a7 = net.time_varying_gain(1, 2, 4, 4)
# print(a7)
