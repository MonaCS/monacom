#!/usr/bin/env python
import numpy as np

numberOfAgent = 7
numberOfState = 2
nf = 4

Stress = np.array([[-0.7524, 0.0684, 0.2052, 0, -0.1368, 0.6156, 0],
                   [0.0684, -0.7524, 0, 0.2052, -0.1368, 0, 0.6156],
                   [0.2052, 0, -0.2052, 0.1026, 0, 0, -0.1026],
                   [0, 0.2052, 0.1026, -0.2052, 0, -0.1026, 0],
                   [-0.1368, -0.1368, 0, 0, -0.1368, 0.2052, 0.2052],
                   [0.6156, 0, 0, -0.1026, 0.2052, -0.7182, 0],
                   [0, 0.6156, -0.1026, 0, 0.2052, 0, -0.7182]])

State = np.ones((numberOfAgent*numberOfState, 1))
Offset = np.ones((numberOfAgent*numberOfState, 1))
velOffset = np.array([[0.0],
                      [0.0]])

def calculateAffineController(numberOfFollower):
    global Command

    stress_ff = Stress[0:numberOfFollower, 0:numberOfFollower]
    stress_fl = Stress[0:numberOfFollower, numberOfFollower:numberOfAgent]
    print stress_ff
    print stress_fl

    invStress_ff = np.linalg.inv(stress_ff)
    invStress_ff_fl = np.matmul(invStress_ff, stress_fl)
    aug_stress_ff_fl = np.kron(invStress_ff_fl, np.eye(numberOfState))
    print invStress_ff_fl

    des_state_l = State[numberOfFollower*numberOfState:numberOfAgent*numberOfState]
    des_state_f = -np.matmul(aug_stress_ff_fl, des_state_l)

    Offset[0 : numberOfFollower*numberOfState] = des_state_f

    aug_stress_ff = np.kron(stress_ff, np.eye(numberOfState))
    state_f = State[0:numberOfFollower*numberOfState]
    delta_state_f = state_f - des_state_f
    Command = np.matmul(aug_stress_ff, delta_state_f)

# Affine controller of agent <i>
def getAffineController(kp, index, velOffset):
    command = kp*Command[numberOfState*index : numberOfState*index+numberOfState] + velOffset
    return command

calculateAffineController(nf)
print Command

ctrl = getAffineController(1, 0, velOffset)
print ctrl
