#!/usr/bin/env python
# Description:
# This rosnode is to execute general consensus-based formation of multi-agent systems.
# Created by:
# Ishak Hilton Pujantoro Tnunay
# PhD candidate in Control System at University of Manchester
# Email: ishak.tnunay@postgrad.manchester.ac.uk
#
# Subscribed topics:
# 1. Position of all robots: /robot_info
#    Message: std_msgs/Float32MultiArray
#
# Published topics:
# 1. Velocity of <i>-th robot: /epuck_robot_<i>/mobile_base/cmd_vel, for <i>=1,...,N
#	 Message: geometry_msgs/Twist

from __future__ import division
from __future__ import print_function
import rospy
import math
import numpy as np
import scipy as sp
import sys
from geometry_msgs.msg import TransformStamped as geoTrStamp
from geometry_msgs.msg import Pose2D as geoPose2D
from geometry_msgs.msg import Twist as geoTwist
from tf.transformations import euler_from_quaternion
from std_msgs.msg import String as stdString
from std_msgs.msg import Float32MultiArray as stdFloat32
import Network_ZH as net
import Save_Read_Files as SRF


# Some constants defined here
factor = 0.1
NRobot = 6
NRobot_follower = 4
RRobot = 0.1 * factor
RR = 0
dim = 2
kp = 0.5
ki = 0
alpha = 14005.6 #0.5 * 100 / factor   #alpha is used to rescale the difference between velocity and encoder ticks
t_wait = 2 # the system will wait t_wait sec before start to behavor.
           # This period gives enough time to measure the inital position of the robot
run_times = 0  # count- the while loop excute


# parameter for pre-specified formation control
gain_a = 0.1*0.9
gain_b = 0.4*0.8
T_settle = 100 #prescribed converge time /sec
# velocity offset of two leaders
leader_index = np.array([[4],
                         [5]])

# velOffset=np.array([[0.005],
#                   [0.00]]) #m/s

velOffset = np.array([[0],
                      [0]])

# Kinematics constrains
limitVel = 0.04 * 1000000  # cm/s
limitOmega = np.pi / 4 * 0.8*100

# designed position
position_d = np.array([[1.30],  # follower 1 x
                       [0.50],  # follower 1 y
                       [1.30],  # follower 2 x
                       [0.00],  # follower 2 y
                       [0.80],  # follower 3 x
                       [0.00],  # follower 3 y
                       [0.80],  # follower 4 x
                       [0.50],  # follower 4 y
                       [1.80],  # leader 1 x
                       [0.50],  # leader 1 y
                       [1.80],  # leader 2 x
                       [0.00]])  # leader 2 y

# obstacles
# ---------------------------
# circle
circle_c = np.array([[0.40],
                     [0.70]])  # center point of circle
radius = 0.15  # radius of circle
activate_range = 0.08
# -------------------------
# trajectory obstacle
line_end = 0.4  # end point of obstacle area

# Robot's info
IDRobot = np.arange(NRobot, dtype=np.uint8)
robotPose = [geoPose2D() for i in range(NRobot)]
robotInput = [geoTwist() for i in range(NRobot)]

update_rate = 60 # 1-80 hz according to the PID level of the mona
# Global variables
u = np.zeros([dim, 1])

# roboInfoCallback Definition
def robotInfoCallback_0(poseData):
    i = 0
    robotPose[i] = geoTrStamp_to_Pose2D(poseData)

def robotInfoCallback_1(poseData):
    i = 1
    robotPose[i] = geoTrStamp_to_Pose2D(poseData)

def robotInfoCallback_2(poseData):
    i = 2
    robotPose[i] = geoTrStamp_to_Pose2D(poseData)

def robotInfoCallback_3(poseData):
    i = 3
    robotPose[i] = geoTrStamp_to_Pose2D(poseData)

def robotInfoCallback_4(poseData):
    i = 4
    robotPose[i] = geoTrStamp_to_Pose2D(poseData)

def robotInfoCallback_5(poseData):
    i = 5
    robotPose[i] = geoTrStamp_to_Pose2D(poseData)

# transfer quaternion to euler angle
def geoTrStamp_to_Pose2D(poseData):
    robotPose2D = geoPose2D()
    orientation_list = [poseData.transform.rotation.x, poseData.transform.rotation.y, poseData.transform.rotation.z,
                        poseData.transform.rotation.w]
    (roll, pitch, yaw) = euler_from_quaternion(orientation_list)
    robotPose2D.theta = yaw
    robotPose2D.x = poseData.transform.translation.x
    robotPose2D.y = poseData.transform.translation.y

    return robotPose2D




def main(args):
    global run_times
    rospy.init_node('mona_formation_ZH_P', anonymous=True)
    # subscribe position from vicon
    subRobotInfo = rospy.Subscriber("vicon/Mona0/Mona0", geoTrStamp, robotInfoCallback_0)  # only need to excute onece
    subRobotInfo = rospy.Subscriber("vicon/Mona1/Mona1", geoTrStamp, robotInfoCallback_1)  # only need to excute onece
    subRobotInfo = rospy.Subscriber("vicon/Mona2/Mona2", geoTrStamp, robotInfoCallback_2)  # only need to excute onece
    subRobotInfo = rospy.Subscriber("vicon/Mona3/Mona3", geoTrStamp, robotInfoCallback_3)  # only need to excute onece
    subRobotInfo = rospy.Subscriber("vicon/Mona4/Mona4", geoTrStamp, robotInfoCallback_4)  # only need to excute onece
    subRobotInfo = rospy.Subscriber("vicon/Mona5/Mona5", geoTrStamp, robotInfoCallback_5)  # only need to excute onece

    pubCommand = [rospy.Publisher("mona%d/cmd_vel" % i, geoTwist, queue_size=1) for i in
                  range(NRobot_follower)]  # only need to excute onece

    rate = rospy.Rate(update_rate)  # 1-hz rate

    net.updateTopology(NRobot, dim)
    # Adj = np.array([[0, 1, 1, 1, 1, 1],
    #                 [1, 0, 1, 0, 1, 0],
    #                 [1, 1, 0, 1, 0, 0],
    #                 [1, 0, 1, 0, 0, 0],
    #                 [1, 0, 0, 0, 0, 1],
    #                 [1, 1, 0, 0, 1, 0]])
    Adj = np.array([[0, 1, 1, 1, 1, 1],
    				[1, 0, 1, 1, 1, 1],
    				[1, 1, 0, 1, 1, 1],
    				[1, 1, 1, 0, 1, 1],
                    [1, 1, 1, 1, 0, 1],
                    [1, 1, 1, 1, 1, 0]])

    # Adj = np.array([[ 0, 1,0, 1],
    # 				[ 1, 0, 1, 1],
    # 				[0, 1, 0, 1],
    # 				[ 1,1, 1, 0]])

    net.setAdjacency(Adj)
    net.setEandG_star(position_d)
    net.setG_star_sum()

    tStart = rospy.get_time()
    mode = 0
    while not rospy.is_shutdown():
        t = (rospy.get_time() - tStart)    #time from the program start
        if (rospy.get_time() - tStart) > t_wait:
            running_time = t - t_wait   #time after the intialization
            SRF.SaveFile.save_file(float(running_time), 'time', './data/P', run_times)


            for i in range(NRobot):
                net.setState_i(i, np.array([[robotPose[i].x], [robotPose[i].y]]))

                SRF.SaveFile.save_file(float(robotPose[i].x), 'X_robot' + str(i), './data/P',
                                       run_times)  # Save the x y and theta data
                SRF.SaveFile.save_file(float(robotPose[i].y), 'Y_robot' + str(i), './data/P', run_times)
                SRF.SaveFile.save_file(float(robotPose[i].theta), 'Theta_robot' + str(i), './data/P', run_times)

            net.setEandG_matrix()
            net.setG_sum()
            net.SetG_proj_matrix()
            net.SetG_proj_sum()

            position_e = net.calPosition_e(position_d)
            SRF.SaveFile.save_file(float(position_e), 'Position_error', './data/P', run_times)

            gain_t_v = net.time_varying_gain(gain_a, gain_b, T_settle, running_time)

            for i in range(NRobot_follower):
                # u = net.getBearingformation_proj(kp, i)
                u = net.getConsensus(i)
                # u = net.getBearingformation_proj(gain_t_v, i)
                ru = net.getControllerKinematics(alpha, u, robotPose[i].theta, RRobot, limitVel, limitOmega)
                robotInput[i].linear.x = ru[0]
                robotInput[i].angular.z = ru[1]

                pubCommand[i].publish(robotInput[i])

                SRF.SaveFile.save_file(float(u[0]), 'X_velocity' + str(i), './data/P', run_times)
                SRF.SaveFile.save_file(float(u[1]), 'Y_velocity' + str(i), './data/P', run_times)
                SRF.SaveFile.save_file(float(ru[0]), 'Linear_v_robot' + str(i), './data/P', run_times)
                SRF.SaveFile.save_file(float(ru[1]), 'Angular' + str(i), './data/P', run_times)

            word = "T: " + str(t) + ", Mode: " + str(mode) + ", Position Error: " + str(position_e) + \
                   ", Time-varying gain: " + str(gain_t_v)
            rospy.loginfo(word)
            run_times = run_times + 1
        rate.sleep()


if __name__ == '__main__':
    main(sys.argv)
