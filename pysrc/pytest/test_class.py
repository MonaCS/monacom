#!/usr/bin/env python

class calc:
    def __init__(self, in_a, in_b):
        self.a = in_a
        self.b = in_b

    def run(self, _operation):
        op = _operation(self)
        self.c = op.calculate()

class add:
    def __init__(self, input):
        self.a = input.a
        self.b = input.b

    def calculate(self):
        self.c = self.a + self.b
        return self.c

clc = calc(5,566)
clc.a = 90
clc.run(add)
print(clc.c)