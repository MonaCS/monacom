#!/usr/bin/env python
# Description:
# This library contains the network setup required for a cooperative controller design.
# Created by:
# 1. Hilton Tnunay (htnunay@gmail.com)
# 2. Zhenhong Li (lizhenhong110@gmail.com)

from __future__ import division
from __future__ import print_function
import math
import numpy as np
import scipy as sp
import sys

class network:
    def __init__(self, _num_node, _num_leader=0, _num_follower=0):
        self.numberOfNode = _num_node    

        self.numberOfLeader = _num_leader
        self.numberOfFollower = _num_follower
        
        self.adjacency = np.zeros([_num_node, _num_node])
        self.inDegree = np.zeros([_num_node, _num_node])
        self.outDegree = np.zeros([_num_node, _num_node])

        self.laplacian = np.zeros([_num_node, _num_node])
        self.stress = np.zeros([_num_node, _num_node])

## FOR TEST!
# a = network(3,4)
# a.laplacian = 2*np.eye(3, 3)
# print(a.laplacian)