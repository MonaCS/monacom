#!/usr/bin/env python
# Description:
# This rosnode is to execute cooperative formation control of multi-agent systems.
# Created by:
# 1. Hilton Tnunay (htnunay@gmail.com)
# 2. Zhenhong Li (lizhenhong110@gmail.com)

from __future__ import division
from __future__ import print_function
import math
import numpy as np
import scipy as sp
import sys


# Update the properties of the network
def updateTopology(N, d):
    global Adjacency
    global Laplacian
    global Stress
    global State
    global Offset
    global Command
    global e_star #differrence of desired positions
    global g_star #bearing star matrix
    global g_star_sum_n #the sum of the bearing vector in neighborhood
    global e_matrix
    global g_matrix
    global g_sum_n
    global integral
    global g_projected        #the projected bearing matrix
    global g_projected_sum_n  #the sum of the projected bearing vector

    global numberOfAgent
    global numberOfState

    numberOfAgent = N
    numberOfState = d

    Adjacency = np.zeros((numberOfAgent, numberOfAgent))
    Laplacian = np.zeros((numberOfAgent, numberOfAgent))
    Stress = np.zeros((numberOfAgent, numberOfAgent))
    State = np.zeros((numberOfAgent*numberOfState, 1))
    Offset = np.zeros((numberOfAgent*numberOfState, 1))
    Command = np.zeros((numberOfAgent*numberOfState, 1))
    e_star = np.zeros((numberOfAgent*numberOfState, numberOfAgent))
    g_star = np.zeros((numberOfAgent*numberOfState, numberOfAgent))
    g_star_sum_n = np.zeros((numberOfAgent*numberOfState, 1))
    e_matrix = np.zeros((numberOfAgent*numberOfState, numberOfAgent))
    g_matrix = np.zeros((numberOfAgent*numberOfState, numberOfAgent))
    g_sum_n = np.zeros((numberOfAgent*numberOfState, 1))
    integral = np.zeros((numberOfAgent*numberOfState, 1))
    g_projected = np.zeros((numberOfAgent*numberOfState, numberOfAgent))
    g_projected_sum_n = np.zeros((numberOfAgent*numberOfState, 1))
    

# update obstacle parameters
def updateObstacles(circle_i, radius_i, line_end_i, activate_range_i):
    global line_end
    global circle_c
    global radius
    global activate_range

    line_end = line_end_i
    circle_c = circle_i
    radius = radius_i
    activate_range = activate_range_i
    #print(line_end, circle_c, radius, activate_range)

# set angle  [-pi pi] to the range [0 2pi)
def setAnglerange(angle):
    if angle < 0:
        angle = angle + 2 * np.pi
    return angle

# set sum or difference of two angles which locates in [-pi pi] to the range [0 2pi]
def setAnglerange_cal(angle):
    if angle < 0:
        angle = angle + 2 * np.pi
    elif  angle >= 2 * np.pi:
        angle = angle -2 *np.pi
    return angle

# switch of line obstacle avoidance
def Switch_lineob(index):
    global State
    global numberOfState
    global line_end
    if State[numberOfState*index] < line_end:
        return 0
    else:
        return 1

#switch of circle obstacle avoidance
def Switch_circleob(distance):
    if distance < radius + activate_range:
        return 1
    else:
        return 0

# Set parameters
def setAdjacency(inAdjacency):
    global Adjacency
    Adjacency = inAdjacency

def setLaplacian(inLaplacian):
    global Laplacian
    Laplacian = inLaplacian

def setStress(inStress):
    global Stress
    Stress = inStress

def setState_i(index, inState):
    global State
    State[numberOfState*index : numberOfState*index+(numberOfState)] = inState

def setOffset_i(index, inOffset):
    global Offset
    Offset[numberOfState*index : numberOfState*index+(numberOfState)] = inOffset

def setOffset(inOffset):
    global Offset
    Offset = inOffset

# Set e_star g_star
def setEandG_star(position_d):
    global e_star
    global g_star
    for j in range(numberOfAgent):
        for i in range(numberOfAgent):
            if i == j:
                e_star[numberOfState*j : numberOfState*j+(numberOfState), i] = [1,1]
            else:
                e_star[numberOfState*j : numberOfState*j+(numberOfState), i] = - position_d[numberOfState*j : numberOfState*j+(numberOfState), 0] + position_d[numberOfState*i : numberOfState*i+(numberOfState), 0]
            g_star[numberOfState*j : numberOfState*j+(numberOfState), i] = e_star[numberOfState*j : numberOfState*j+(numberOfState), i] / np.linalg.norm(e_star[numberOfState*j : numberOfState*j+(numberOfState), i])
    #print(g_star)

#Set e and g matrix
def setEandG_matrix():
    global State
    global e_matrix
    global g_matrix
    for j in range(numberOfAgent):
        for i in range(numberOfAgent):
            if i == j:
                e_matrix[numberOfState*j : numberOfState*j+(numberOfState), i] = [1, 1]
            else:
                e_matrix[numberOfState*j : numberOfState*j+(numberOfState), i] = - State[numberOfState*j : numberOfState*j+(numberOfState), 0] + State[numberOfState*i : numberOfState*i+(numberOfState), 0]
            g_matrix[numberOfState*j : numberOfState*j+(numberOfState), i] = e_matrix[numberOfState*j : numberOfState*j+(numberOfState), i] / np.linalg.norm(e_matrix[numberOfState*j : numberOfState*j+(numberOfState), i])
    #print(g_matrix)
    #print(e_matrix)
    #print(State)

#Set projected g matrix
def SetG_proj_matrix():
    global g_projected        #the projected bearing matrix

    for j in range(numberOfAgent):
        for i in range(numberOfAgent):
            g_projected[numberOfState*j : numberOfState*j+(numberOfState), i] = np.matmul( bearing_projection(g_matrix[numberOfState*j : numberOfState*j+(numberOfState), i]),
                                                                                           g_star[numberOfState*j : numberOfState*j+(numberOfState), i])

#Set progected bearing vector
def SetG_proj_sum():
    global g_projected_sum_n

    g_projected_sum_n_local = np.zeros((numberOfAgent*numberOfState,1))
    for j in range(numberOfAgent):
        for i in range(numberOfAgent):
            if not Adjacency[j, i] == 0:
                g_projected_sum_n_local[numberOfState*j : numberOfState*j + (numberOfState), 0] = \
                    g_projected_sum_n_local[numberOfState*j : numberOfState*j + (numberOfState), 0] + \
                    g_projected[numberOfState*j : numberOfState*j + (numberOfState), i]

    g_projected_sum_n = g_projected_sum_n_local

# Set g_star_sum_n (the sum of neiboring bearing vectors)
def setG_star_sum():
    global Adjacency
    global g_star_sum_n

    g_star_sum_n_local = np.zeros((numberOfAgent*numberOfState,1))
    for j in range(numberOfAgent):
        for i in range(numberOfAgent):
            if not Adjacency[j,i] == 0:
                g_star_sum_n_local[numberOfState*j : numberOfState*j + (numberOfState), 0] = g_star_sum_n_local[numberOfState*j : numberOfState*j + (numberOfState), 0] + g_star[numberOfState*j : numberOfState*j + (numberOfState), i]

    g_star_sum_n = g_star_sum_n_local
    #print(g_star_sum_n)

# Set g_sum_n
def setG_sum():
    global Adjacency
    global g_sum_n

    g_sum_n_local = np.zeros((numberOfAgent*numberOfState,1))
    for j in range(numberOfAgent):
        for i in range(numberOfAgent):
            if not Adjacency[j,i] == 0:
                g_sum_n_local[numberOfState*j : numberOfState*j + (numberOfState), 0] =  g_sum_n_local[numberOfState*j : numberOfState*j + (numberOfState), 0] + g_matrix[numberOfState*j : numberOfState*j + (numberOfState), i]

    g_sum_n = g_sum_n_local
    #print(g_sum_n)



# Calculate bearing error
def calBearing_e():
    global g_matrix
    global g_star
    bearing_e = 0.0
    bearing_e_matrix = g_matrix - g_star
    for j in range(numberOfAgent):
        for i in range(numberOfAgent):
            if not Adjacency[j,i] == 0:
                bearing_e = bearing_e + np.linalg.norm(bearing_e_matrix[numberOfState*j : numberOfState*j+(numberOfState), i])
    #print(bearing_e)
    return(bearing_e)

# Calculate position error
def calPosition_e(position_d):
    position_e = State - position_d
    position_e_norm = np.linalg.norm(position_e, 2)
    return position_e_norm



# Get parameters
def getAdjacency():
    return Adjacency

def getLaplacian():
    return Laplacian

# Formation transformation
def doTransformation(index, A, p, b):
    global Offset
    Offset[numberOfState*index : numberOfState*index+(numberOfState)] = np.matmul(A,p) + b

def getRotationMat2D(inTheta):
    theta = math.radians(inTheta)
    return np.array([[math.cos(theta), -math.sin(theta)],[math.sin(theta), math.cos(theta)]])

# Simple proportional controller of agent <i>
def getProportional(index, dP):
    state = State[numberOfState*index : numberOfState*index+(numberOfState)]
    desPosition = dP[numberOfState*index : numberOfState*index+(numberOfState)]
    error = state - desPosition
    K = np.array([[30, 0],
                  [ 0,30]])
    Command = -np.matmul(K,error)
    return Command

#Simple bearing based projection controller of agent <i>
def getBearingformation_proj(kp, index):
    u = np.zeros([numberOfState, 1])
    u = - kp * np.matmul(np.array([[1.5,0],[0,1.5]]),  g_projected_sum_n[numberOfState * index : numberOfState * index + numberOfState])
    #u = - kp * g_projected_sum_n[numberOfState * index : numberOfState * index + numberOfState]
    return u

#Simple bearing based formation proportional controller of agnt <i>
def getBearingformation_p(kp, index, leader_index, velOffset=np.array([[0],
                                                     [0]]) ):
    global g_sum_n
    global g_star_sum_n
    u = np.zeros([numberOfState,1])
    #if index in leader_index:

    if index == 0:
        Command = velOffset + 0*0.8*kp*(State[2:4]-State[0:2] + np.array([[0],[0.2]]))
    elif index == 1:
        Command = velOffset + 0*0.8*kp*(State[0:2]-State[2:4] - np.array([[0],[0.2]]))
    else:
        u = kp * (g_sum_n[numberOfState*index : numberOfState*index+(numberOfState)] - g_star_sum_n[numberOfState*index : numberOfState*index+(numberOfState)] )
        Command = u
    #print(g_sum_n,g_star_sum_n)
    return Command


# Simple bearing based formation PI controllaer of agent <i>
def getBearingformation_pi(kp, ki, update_rate, index, leader_index, velOffset=np.array([[0],
                                                     [0]]) ):
    global g_sum_n
    global g_star_sum_n
    global integral

    if np.any(np.isnan(integral)):
        integral = np.zeros((numberOfAgent*numberOfState,1))

    u = np.zeros([numberOfState,1])
    difference_g = np.zeros([numberOfState,1])

    if index == 0:
        Command = velOffset + 0.8*kp*(State[2:4]-State[0:2] + np.array([[0],[0.2]]))
    elif index == 1:
        Command = velOffset + 0.8*kp*(State[0:2]-State[2:4] - np.array([[0],[0.2]]))
    else:
        difference_g = g_sum_n[numberOfState*index : numberOfState*index+(numberOfState)] - g_star_sum_n[numberOfState*index : numberOfState*index+(numberOfState)]
        u = kp * difference_g + ki * integral[numberOfState*index : numberOfState*index+(numberOfState)]
        # print(0.01*difference_g)

        # integral[numberOfState*index : numberOfState*index+(numberOfState)] = 0.1*difference_g #/update_rate*difference_g
        # integral_rate = difference_g
        # integral[numberOfState*index : numberOfState*index+(numberOfState)] = np.add(integral[numberOfState*index : numberOfState*index+(numberOfState)], difference_g)
        integral[numberOfState*index : numberOfState*index+(numberOfState)] = integral[numberOfState*index : numberOfState*index+(numberOfState)] + 0.05*difference_g #+ 0.01*difference_g #

        bound_int = 0.83
        if integral[numberOfState*index] > bound_int:
            integral[numberOfState*index] = bound_int
        elif integral[numberOfState*index] < -bound_int:
            integral[numberOfState*index] = -bound_int

        if integral[numberOfState*index+1] > 0.2*bound_int:
            integral[numberOfState*index+1] = 0.2*bound_int
        elif integral[numberOfState*index+1] < -0.2*bound_int:
            integral[numberOfState*index+1] = -0.2*bound_int

        Command = u

        # print(integral[numberOfState*index : numberOfState*index+(numberOfState)])
    #print(g_sum_n,g_star_sum_n)
    # print(difference_g)
    # print(integral)
    return Command

# def getBearingformation_proj(kp, index):
#     u = np.zeros([numberOfState, 1])
#     u = - kp * np.matmul(np.array([[1.5,0],[0,1.5]]),  g_projected_sum_n[numberOfState * index : numberOfState * index + numberOfState])
#     #u = - kp * g_projected_sum_n[numberOfState * index : numberOfState * index + numberOfState]
#     return u

# Simple consensus controller of agent <i>
def getConsensus(kp, index, velOffset):
    u = np.zeros([numberOfState,1])
    delta_state = np.zeros([numberOfState,1])
    state_hat_i = np.zeros([numberOfState,1])
    state_hat_j = np.zeros([numberOfState,1])

    state_hat_i = State[numberOfState*index : numberOfState*index+(numberOfState)] - Offset[numberOfState*index : numberOfState*index+(numberOfState)]
    for j in range(numberOfAgent):
        state_hat_j = State[numberOfState*j : numberOfState*j+(numberOfState)] - Offset[numberOfState*j : numberOfState*j+(numberOfState)]
        delta_state = state_hat_i - state_hat_j
        u = u + np.matmul(Adjacency[index, j], delta_state)
    Command = -kp*u	+ velOffset
    # print "k ", Command
    return Command

# Calculate Affine formation controller
def calculateAffineController(numberOfFollower):
    global Command
    global Offset

    stress_ff = Stress[0:numberOfFollower, 0:numberOfFollower]
    stress_fl = Stress[0:numberOfFollower, numberOfFollower:numberOfAgent]
    # print stress_ff
    # print stress_fl

    invStress_ff = np.linalg.inv(stress_ff)
    invStress_ff_fl = np.matmul(invStress_ff, stress_fl)
    aug_stress_ff_fl = np.kron(invStress_ff_fl, np.eye(numberOfState))
    # print invStress_ff_fl

    des_state_l = State[numberOfFollower*numberOfState:numberOfAgent*numberOfState]
    des_state_f = -np.matmul(aug_stress_ff_fl, des_state_l)

    Offset[0 : numberOfFollower*numberOfState] = des_state_f
    Offset[numberOfFollower*numberOfState:numberOfAgent*numberOfState] = State[numberOfFollower*numberOfState:numberOfAgent*numberOfState]

    aug_stress_ff = np.kron(stress_ff, np.eye(numberOfState))
    state_f = State[0:numberOfFollower*numberOfState]
    delta_state_f = state_f - des_state_f
    Command = np.matmul(aug_stress_ff, delta_state_f)

# Affine formation controller of agent <i>
def getAffineController(kp, index, velOffset):
    command = kp*Command[numberOfState*index : numberOfState*index+numberOfState] + velOffset
    return command

# Calculate state error
def calculateStateError():
    position_e = State - Offset
    position_e_norm = np.linalg.norm(position_e, 2)
    return position_e_norm

def getControllerKinematics(alpha, beta, u, theta, RRobot, limitVel, limitOmega):
    #limitVel = 5
    #limitOmega = 2.50

    v = alpha*u[0]*math.cos(theta) + alpha*u[1]*math.sin(theta)
    w = beta*(-(u[0]/RRobot)*math.sin(theta) + (u[1]/RRobot)*math.cos(theta))

    # print np.array([[v], [w]])
    if v > limitVel:
        v = limitVel
    elif v < -limitVel:
        v = -limitVel
    if w > limitOmega:
        w = limitOmega
    elif w < -limitOmega:
        w = -limitOmega

    return np.array([[v], [w]])

# omega of agent in the line obstacle
def setOmega_line_obstacle(index, Omega_original, theta, RRobot):
    global State
    global line_end

    Omega = Omega_original
    if State[numberOfState*index] < line_end:
        #Omega= -(1/RRobot)*math.sin(theta) + (0/RRobot)*math.cos(theta)
         Omega=0.01
    return Omega

# omega of agent for the circle obstacle
def setOmega_circle_obstacle(u, Omega_original, theta, index, RRobot):
    global State
    global circle_c
    global radius
    global activate_range

    distance = np.linalg.norm( State[numberOfState*index : numberOfState*index+(numberOfState)]- circle_c ) # distance between current posiution and center

    if distance >= radius + activate_range:  #avoidance not activate
        return Omega_original

    else:
        unit_oritation = (circle_c - State[numberOfState*index : numberOfState*index+(numberOfState)]) / distance #unit vector from current position to center
        sine_angle = radius / distance
        if sine_angle > 1:
            sine_angle = 1
        # if the position is inside the circle, it will verticly move out
        angle_1 = math.asin(sine_angle)
        angle_2 = math.atan2(unit_oritation[1], unit_oritation[0])
        angle_2 = setAnglerange(angle_2) #angle of unit vector in range [0 2*pi)

        angle_bound_1 = angle_2 - angle_1
        angle_bound_1 = setAnglerange_cal(angle_bound_1) # the vector in the tangent cone of obstacle in range [0 2*pi)
        angle_bound_2 = angle_2 + angle_1
        angle_bound_2 = setAnglerange_cal(angle_bound_2) # the vector in the tangent cone of obstacle in range [0 2*pi)

        norm_u = np.linalg.norm(u)
        unit_oritation_2 = u / norm_u #the heading of f
        unit_oritation_3 = - unit_oritation_2 #the heading of -f

        angle_3 = math.atan2(unit_oritation_2[1], unit_oritation_2[0])
        angle_3 = setAnglerange(angle_3) #agnle of unit vector f [0 2*pi]

        angle_3_negative = math.atan2(unit_oritation_3[1], unit_oritation_3[0])
        angle_3_negative = setAnglerange(angle_3_negative) #angle of unit vector -f [0 2*pi]

        angle_4 = np.abs(angle_3 - angle_bound_1) #angle between f and minus bound
        angle_5 = np.abs(angle_3 - angle_bound_2)

        angle_4_negative = np.abs(angle_3_negative - angle_bound_1)
        angle_5_negative = np.abs(angle_3_negative - angle_bound_2)

        h_d = u #inital designed heading
        angle_low = min(angle_bound_1, angle_bound_2) #lower bound of tangent cone
        angle_high = max(angle_bound_1, angle_bound_2) #high bound of tangent cone

        if State[numberOfState*index] > circle_c[0] and State[numberOfState*index + 1] > circle_c[1]: # if locate in area 1
            if angle_3 >= angle_low and angle_3 <= angle_high:
                if angle_4 <= angle_5:
                    h_d = np.array([[math.cos(angle_bound_1)],
                                    [math.sin(angle_bound_1)]])*norm_u
                else:
                    h_d =  np.array([[math.cos(angle_bound_2)],
                                    [math.sin(angle_bound_2)]])*norm_u

        elif State[numberOfState*index] <= circle_c[0] and State[numberOfState*index + 1] >= circle_c[1]: # if locate in area 2
            if angle_3_negative >= angle_low and angle_3_negative <= angle_high:
                if angle_4_negative > angle_5_negative:
                    h_d = np.array([[math.cos(angle_bound_1)],
                                    [math.sin(angle_bound_1)]])*norm_u
                else:
                    h_d = np.array([[math.cos(angle_bound_2)],
                                    [math.sin(angle_bound_2)]])*norm_u

        elif State[numberOfState*index] < circle_c[0] and State[numberOfState*index + 1] < circle_c[1]: #if locate in area 3
            if angle_3_negative >= angle_low and angle_3_negative <= angle_high:
                if angle_4_negative > angle_5_negative:
                    h_d = np.array([[math.cos(angle_bound_1)],
                                    [math.sin(angle_bound_1)]])*norm_u
                else:
                    h_d = np.array([[math.cos(angle_bound_2)],
                                    [math.sin(angle_bound_2)]])*norm_u

        elif State[numberOfState*index] >= circle_c[0] and State[numberOfState*index + 1] < circle_c[1]: #if locate in area 4
            if angle_3 >= angle_low and  angle_3 <= angle_high:
                if angle_4 <= angle_5:
                    h_d = np.array([[math.cos(angle_bound_1)],
                                    [math.sin(angle_bound_1)]])*norm_u
                else:
                    h_d =  np.array([[math.cos(angle_bound_2)],
                                    [math.sin(angle_bound_2)]])*norm_u

        Omega = -(h_d[0]/RRobot)*math.sin(theta) + (h_d[1]/RRobot)*math.cos(theta)
        return Omega

def getControllerKinematrics_acoidance(alpha, u, theta, RRobot, limitVel, limitOmega, index, index_trajectory_obstacle, index_circle_obstacle):
    global State
    global line_end

    v = alpha*u[0]*math.cos(theta) + alpha*u[1]*math.sin(theta)
    w = -(u[0]/RRobot)*math.sin(theta) + (u[1]/RRobot)*math.cos(theta)


    #with avoidence
    if index == index_trajectory_obstacle:  #avoidance of straight line
        w = setOmega_line_obstacle(index, w, theta, RRobot)

    if index == index_circle_obstacle:   #avoidance of circle
        w = setOmega_circle_obstacle(u, w, theta, index, RRobot)

        # print np.array([[v], [w]])
    if v > limitVel:
            v = limitVel
    elif v < -limitVel:
            v = -limitVel
    if w > limitOmega:
            w = limitOmega
    elif w < -limitOmega:
            w = -limitOmega

    return np.array([[v], [w]])

def bearing_projection(bearing):
    #in python 1 dim vector is recognised as raw vector
    bearing_col = bearing.reshape(-1, 1)
    Porjection_x = np.identity(numberOfState) - np.matmul(bearing_col, bearing_col.transpose()) / np.matmul(bearing_col.transpose(), bearing_col)

    return Porjection_x

# time_varying_gain is used for pro-specified finite time controller
def time_varying_gain(gain_a, gain_b, settle_time,  running_time):
    t_len = running_time - settle_time

    if t_len < 0:
        gain_out = gain_a + gain_b * settle_time / np.abs(t_len)
    else:
        gain_out = gain_a

    return gain_out
