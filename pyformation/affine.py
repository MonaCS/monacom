#!/usr/bin/env python
# Description:
# This rosnode is to execute general affine-consensus-based formation of multi-agent systems.
# Created by:
# Hilton Tnunay (htnunay@gmail.com)
# Okechi Onuoha

from __future__ import division
from __future__ import print_function
import rospy
import math
import numpy as np
import sys
from geometry_msgs.msg import TransformStamped as geoTrStamp
from geometry_msgs.msg import Pose2D as geoPose2D
from geometry_msgs.msg import Twist as geoTwist
from tf.transformations import euler_from_quaternion
from std_msgs.msg import String as stdString
from std_msgs.msg import Float32MultiArray as stdFloat32
import Network as net
from turtlesim.msg import Pose as turtPose

# Some constants defined here
factor = 10
NRobot = 7
NRobot_leader = 3
NRobot_follower = 4
RRobot = 0.1 * factor
RR = 0
dim = 2
kp = 0.1
ki = 6
alpha = 6.5 
beta = 20.5
t_wait = 2 # the system will wait t_wait sec before start to move.
           # This period gives enough time to measure the inital position of the robot
run_times = 0  # count- the while loop excute

# velocity offset of two leaders
leader_index = np.array([4,5,6])

# velOffset=np.array([[0.005],
#                   [0.00]]) #m/s

velOffset = np.array([[0.0],
                      [0.0]])

# Kinematics constrains
limitVel = 0.4 * 1000000  # cm/s
limitOmega = np.pi / 4 * 0.8*1000000

# designed position
position_d = np.zeros([NRobot*dim,1])

# Robot's info
IDRobot = np.arange(NRobot, dtype=np.uint8)
robotPose = [geoPose2D() for i in range(NRobot)]
robotInput = [geoTwist() for i in range(NRobot)]

update_rate = 60 # 60Hz
# Global variables
u = np.zeros([dim, 1])

# roboInfoCallback Definition
def robotInfoCallback_0(poseData):
    i = 0
    robotPose[i] = getPose2D(poseData)

def robotInfoCallback_1(poseData):
    i = 1
    robotPose[i] = getPose2D(poseData)

def robotInfoCallback_2(poseData):
    i = 2
    robotPose[i] = getPose2D(poseData)

def robotInfoCallback_3(poseData):
    i = 3
    robotPose[i] = getPose2D(poseData)

def robotInfoCallback_4(poseData):
    i = 4
    robotPose[i] = getPose2D(poseData)

def robotInfoCallback_5(poseData):
    i = 5
    robotPose[i] = getPose2D(poseData)

def robotInfoCallback_6(poseData):
    i = 6
    robotPose[i] = getPose2D(poseData)

# transfer quaternion to euler angle
def getPose2D(poseData):
    robotPose2D = geoPose2D()    
    robotPose2D.theta = poseData.theta
    robotPose2D.x = poseData.x
    robotPose2D.y = poseData.y
    return robotPose2D

def main(args):
    global run_times
    rospy.init_node('affine', anonymous=True)

    # subscribe position from vicon
    subRobotInfo = rospy.Subscriber("turtle1/pose", turtPose, robotInfoCallback_0)
    subRobotInfo = rospy.Subscriber("turtle2/pose", turtPose, robotInfoCallback_1)
    subRobotInfo = rospy.Subscriber("turtle3/pose", turtPose, robotInfoCallback_2)
    subRobotInfo = rospy.Subscriber("turtle4/pose", turtPose, robotInfoCallback_3)
    subRobotInfo = rospy.Subscriber("turtle5/pose", turtPose, robotInfoCallback_4)
    subRobotInfo = rospy.Subscriber("turtle6/pose", turtPose, robotInfoCallback_5)
    subRobotInfo = rospy.Subscriber("turtle7/pose", turtPose, robotInfoCallback_6)

    pubCommand = [rospy.Publisher("turtle%d/cmd_vel" % (i+1), geoTwist, queue_size=1) for i in range(NRobot_follower)]

    rate = rospy.Rate(update_rate)  # ros update rate

    net.updateTopology(NRobot, dim)    
    Stress = np.array([[-0.7524, 0.0684, 0.2052, 0, -0.1368, 0.6156, 0],
                       [0.0684, -0.7524, 0, 0.2052, -0.1368, 0, 0.6156],
                       [0.2052, 0, -0.2052, 0.1026, 0, 0, -0.1026],
                       [0, 0.2052, 0.1026, -0.2052, 0, -0.1026, 0],
                       [-0.1368, -0.1368, 0, 0, -0.1368, 0.2052, 0.2052],
                       [0.6156, 0, 0, -0.1026, 0.2052, -0.7182, 0],
                       [0, 0.6156, -0.1026, 0, 0.2052, 0, -0.7182]])
    
    net.setStress(Stress)
    net.setOffset(position_d)    

    tStart = rospy.get_time()
    while not rospy.is_shutdown():
        t = (rospy.get_time() - tStart)    # time from the program start
        if t > t_wait:
            for i in range(NRobot):
                net.setState_i(i, np.array([[robotPose[i].x], [robotPose[i].y]]))
            
            net.calculateAffineController(NRobot_follower)
            position_e = net.calculateStateError()            

            for i in range(NRobot_follower):                
                u = net.getFormationControlCommand(i, kp, velOffset)
                ru = net.getControllerKinematics(alpha, beta, u, robotPose[i].theta, RRobot, limitVel, limitOmega)
                robotInput[i].linear.x = ru[0]
                robotInput[i].angular.z = ru[1]

                pubCommand[i].publish(robotInput[i])

            word = "T: " + str(t) + ", Position Error: " + str(position_e)
            rospy.loginfo(word)            
        rate.sleep()


if __name__ == '__main__':
    main(sys.argv)
