#!/usr/bin/env python
# Description:
# This rosnode is to execute general consensus-based formation of multi-agent systems.
# Created by:
# Hilton Tnunay (htnunay@gmail.com)

from __future__ import division
from __future__ import print_function
import rospy
import math
import numpy as np
import sys
from geometry_msgs.msg import TransformStamped as geoTrStamp
from geometry_msgs.msg import Pose2D as geoPose2D
from geometry_msgs.msg import Twist as geoTwist
from tf.transformations import euler_from_quaternion
from std_msgs.msg import String as stdString
from std_msgs.msg import Float32MultiArray as stdFloat32
import Network as net
from turtlesim.msg import Pose as turtPose

# Some constants defined here
factor = 10
NRobot = 4
NRobot_leader = 3
NRobot_follower = 4
RRobot = 0.1 * factor
RR = 0
dim = 2
kp = 0.1
ki = 6
alpha = 1.5 #0.5 * 100 / factor   #alpha is used to rescale the difference between velocity and encoder ticks
beta = 2.5
t_wait = 2 # the system will wait t_wait sec before start to move.
           # This period gives enough time to measure the inital position of the robot
run_times = 0  # count- the while loop excute


velOffset = np.array([[0.0],
                      [0.0]])

# Kinematics constrains
limitVel = 0.04 * 1000000  # cm/s
limitOmega = np.pi / 4 * 0.8*10000000

# designed position
position_d = np.zeros([NRobot*dim,1])

# position_d = np.array([[7.50],
#                        [7.50],
#                        [7.50],
#                        [3.50],
#                        [3.50],
#                        [3.50],
#                        [3.50],
#                        [7.50]])

# Robot's info
IDRobot = np.arange(NRobot, dtype=np.uint8)
robotPose = [geoPose2D() for i in range(NRobot)]
robotInput = [geoTwist() for i in range(NRobot)]

update_rate = 60 # 1-80 hz according to the PID level of the mona
# Global variables
u = np.zeros([dim, 1])

# roboInfoCallback Definition
def robotInfoCallback_0(poseData):
    i = 0
    robotPose[i] = getPose2D(poseData)

def robotInfoCallback_1(poseData):
    i = 1
    robotPose[i] = getPose2D(poseData)

def robotInfoCallback_2(poseData):
    i = 2
    robotPose[i] = getPose2D(poseData)

def robotInfoCallback_3(poseData):
    i = 3
    robotPose[i] = getPose2D(poseData)

def getPose2D(poseData):
    robotPose2D = geoPose2D()    
    robotPose2D.theta = poseData.theta
    robotPose2D.x = poseData.x
    robotPose2D.y = poseData.y
    return robotPose2D

def main(args):
    global run_times
    rospy.init_node('consensus', anonymous=True)

    # subscribe position from vicon
    subRobotInfo = rospy.Subscriber("turtle1/pose", turtPose, robotInfoCallback_0)
    subRobotInfo = rospy.Subscriber("turtle2/pose", turtPose, robotInfoCallback_1)
    subRobotInfo = rospy.Subscriber("turtle3/pose", turtPose, robotInfoCallback_2)
    subRobotInfo = rospy.Subscriber("turtle4/pose", turtPose, robotInfoCallback_3)

    pubCommand = [rospy.Publisher("turtle%d/cmd_vel" % (i+1), geoTwist, queue_size=1) for i in range(NRobot)]

    rate = rospy.Rate(update_rate)  # ros update rate

    net.updateTopology(NRobot, dim)
    
    Lapl = np.array([[3., -1., -1., -1.],
    				 [-1., 3., -1., -1.],
    				 [-1., -1., 3., -1.],
    				 [-1., -1., -1., 3.]])

    net.setLaplacian(Lapl)    
    net.setOffset(position_d)
    
    tStart = rospy.get_time()    
    while not rospy.is_shutdown():
        t = (rospy.get_time() - tStart)    # time from the program start
        if t > t_wait:            
            for i in range(NRobot):
                net.setState_i(i, np.array([[robotPose[i].x], [robotPose[i].y]]))

            net.calculateConsensusController()
            position_e = net.calculateStateError()

            for i in range(NRobot):                
                u = net.getFormationControlCommand(i, kp, velOffset)
                ru = net.getControllerKinematics(alpha, beta, u, robotPose[i].theta, RRobot, limitVel, limitOmega)
                robotInput[i].linear.x = ru[0]
                robotInput[i].angular.z = ru[1]

                pubCommand[i].publish(robotInput[i])                

            word = "T: " + str(t) + ", Position Error: " + str(position_e)
            rospy.loginfo(word)            
        rate.sleep()


if __name__ == '__main__':
    main(sys.argv)
